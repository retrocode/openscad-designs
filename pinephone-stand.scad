// A Stevenson
// Tolerance test v2
module pinephone() {
    width=80.6;
    height=160;
    depth=12.40;
    translate([-(width/2), -(depth/2), 10])
        rotate([-30,0,0])
            cube([width, depth, height],false);
}

module base() {
    width=90;
    depth=100;
    height=30;
    color("yellow")
        translate([0,depth/3,height/2])
            cube([width,depth,height], true);
}

module logo() {
     import("pine64-logo-color.svg", center=true);
}

module stand() {
    intersection() {
            
        difference() {
            base();
            pinephone();
            // Front cut-out
            translate([-25,-25,-1])
                cube([50,50,40]);
            // Back cut-out
            translate([-51,30,5])
                cube([102,60,60]);
            translate([0,53,3])
                linear_extrude(height=5)
                    logo();

        }
        
        sphere(r=300);
}

stand();

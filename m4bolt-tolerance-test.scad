// A Stevenson
// Tolerance test v2
module m4bolt() {
    boltHeadRadius=7.4/2; // bolt = 6.84
    boltHeadDepth=4.2; // bolt = 4.2
    boltLength=34.2;
    boltRadius=4.2/2; // bolt = 3.9, but 4.2 for screwing
    cylinder(boltHeadDepth,boltHeadRadius,boltHeadRadius,false);
    cylinder(boltLength,boltRadius,boltRadius,false);
}

module base() {
    width=20;
    depth=20;
    height=6;
    translate([0,0,height/2])
        cube([width,depth,height], true);
}

translate([0,0,6]) {
    rotate([180,0,0]) {
        difference() {
            base();
            m4bolt();
        }
    }
}
